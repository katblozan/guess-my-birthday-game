from random import randint

#gets name from user and stores.
name=input("Hi! What is your name? ")

#creates for loop and random values for month and year.
for guess_number in range(1,6):
    month=randint(1,12)
    year=randint(1924,2004)
    #our output for the user
    print("Guess", guess_number, ": ", name, " were you born in ", month, " / ", year, "?")
    response=input("yes or no? ")
    #if response is yes..
    if (response=="yes"):
        print("I knew it!")
        #..exits the application
        exit()
    #if we guess >5x..
    elif (guess_number==5):
        print("I have other things to do. Good bye.")
        #..exits..
        exit()
    #our message if we can guess again.
    else:
        print("Drat! Lemme try again!")
